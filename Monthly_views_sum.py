import collections
import functools
import operator
from datetime import date, timedelta
import getPageViews

def getMonthlyViews():
    """
    Sum 30 days total and sort by most viewed by getPageViews function output.
    :return: Dictonary sorted by most viewed articals.
    """
    temp_page_views = []
    PROJECT = "en.wikipedia.org"
    days_back = 30
    LAST_DAY = 0
    while days_back != LAST_DAY:
        delta_days = timedelta(days=-days_back)
        date_to_calc = date.today() + delta_days
        resp = getPageViews.getPageViews(PROJECT, date_to_calc)
        if(resp["exit_code"] == -1):
            return resp["msg"]
        page_views = resp["msg"]
        temp_page_views.append(page_views)
        days_back -= 1
    dict_page_views = dict(sorted(functools.reduce(operator.add, map(
        collections.Counter, temp_page_views)).items(), key=lambda item: item[1], reverse=True))
    return dict_page_views