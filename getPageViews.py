from asyncio import constants
import requests
import json
from datetime import date, timedelta


USER_AGENT = "ronennt@gmail.com"
HOST = "https://wikimedia.org/api/rest_v1/metrics/pageviews"


def destructDate(_date):
    day = _date.strftime("%d")
    month = _date.strftime("%m")
    year = _date.strftime("%Y")
    return (day, month, year)


def getPageViews(_proj, _date):
    """
    Get the count of page view date and create map of article_name: page_views
    Arguments:
    _proj: Wipipedia project name, e.g. "en.wikipedia.org".
    _date: datetime object

    Returns:
    map of article_name: page_views
    """
    try:
        if not isinstance(_date, date):
            raise TypeError(f'{_date} is NOT a date object')
        day, month, year = destructDate(_date)
        url = f"{HOST}/top/{_proj}/all-access/{year}/{month}/{day}"
        response = requests.get(url, headers={"User-Agent": USER_AGENT})
        if response.status_code != 200:
            raise Exception(json.loads(response.content)["detail"])
        articles = response.json()["items"][0]["articles"]
        articles_dict = dict()
        for item in articles:
            articles_dict[item["article"]] = item["views"]
        articles_dict.pop("Main_Page", None)
        articles_dict.pop("Special:Search", None)
        return {"exit_code": 0, "msg": articles_dict}
    except Exception as e:
        return({"exit_code": -1, "msg": e})
